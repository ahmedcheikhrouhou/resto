import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appBackground]'
})
export class BackgroundDirective {

  constructor(private el : ElementRef) { }

@HostListener('mouseenter') onMouseEnter(){
this.el.nativeElement.style.backgroundColor='green';
}
@HostListener('mouseleave') onMouseLeave(){
  this.el.nativeElement.style.backgroundColor=null;
  }


}

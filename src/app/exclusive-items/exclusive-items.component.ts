import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { PlatService } from '../services/plat.service';

@Component({
  selector: 'app-exclusive-items',
  templateUrl: './exclusive-items.component.html',
  styleUrls: ['./exclusive-items.component.css']
})
export class ExclusiveItemsComponent implements OnInit {
  plats:Plat[];

  constructor(private platService:PlatService) { }

  ngOnInit(): void {
    this.platService.getPlats().subscribe(
      data => {
        console.log("Data", data);
        this.plats = data;
      })
  }

}

import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-chefs',
  templateUrl: './chefs.component.html',
  styleUrls: ['./chefs.component.css']
})
export class ChefsComponent implements OnInit {

  chefs:Chef[]
  constructor(private chefService:ChefService) { }

  ngOnInit(): void {
    this.chefService.getChefs().subscribe(
      data => {
        console.log("Data",data);
        this.chefs=data;
      }
    )
  
  }

}

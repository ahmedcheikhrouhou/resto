import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { AdminComponent } from './admin/admin.component';
import { AddPlatComponent } from './add-plat/add-plat.component';
import { DisplayMenuComponent } from './display-menu/display-menu.component';
import { DisplayChefComponent } from './display-chef/display-chef.component';
import { AddChefComponent } from './add-chef/add-chef.component';
import { EditPlatComponent } from './edit-plat/edit-plat.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { DisplayUserComponent } from './display-user/display-user.component';
import { AuthGuardService } from './services/auth-guard.service';
import { DataComponent } from './data/data.component';



const routes: Routes = [
  {
    path:'sign-in',
    component:SigninComponent
  },
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'sign-up',
    component:SignupComponent
  },
  {
    path:'about', component:AboutComponent
  },
  {
    path:'contact', component:ContactComponent
  },
  {
    path:'admin', component:AdminComponent , canActivate :[AuthGuardService]
  },
  {
    path:'addPlat',component:AddPlatComponent , canActivate :[AuthGuardService]
  },
  {
    path:'displayMenu/:id',component:DisplayMenuComponent
  },
  {
    path:'displayChef/:id',component:DisplayChefComponent
  },
  {
    path:'addChef',component:AddChefComponent
  },
  {
    path:'editPlat/:id',component:EditPlatComponent
  },
  {
    path:'addUser',component:AddUserComponent
  },
  {
    path:'editChef/:id',component:EditChefComponent
  },
  {
    path:'editUser/:id',component:EditUserComponent
  },
  {
    path:'displayUser/:id',component:DisplayUserComponent
  },
  {
    path:'data' , component:DataComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

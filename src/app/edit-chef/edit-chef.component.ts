import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chef } from '../models/chef';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-edit-chef',
  templateUrl: './edit-chef.component.html',
  styleUrls: ['./edit-chef.component.css']
})
export class EditChefComponent implements OnInit {

  id:string;
  chef:Chef;
  constructor( private activatedRoute:ActivatedRoute,
    private chefService:ChefService,
    private router:Router) { }

  ngOnInit(): void {
    this.id=this.activatedRoute.snapshot.paramMap.get('id');
    this.chefService.getChefById(this.id).subscribe(
      data=> {
        this.chef=data[0]; 
      }
    )
  }

  editChef(p:any){
    this.chefService.editChef(this.chef).subscribe(
      ()=>
      this.router.navigate(['/admin'])
    )
  }
}

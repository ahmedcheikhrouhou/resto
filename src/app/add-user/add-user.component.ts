import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MustMatch } from '../validators/mustmatchvalidator';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../models/User';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user:User;
  userForm: FormGroup;
  constructor( 
    private fb: FormBuilder,
    private userService:UserService,
    private router:Router
   ) { 
    this.user=new User(0,'','','',0,'','');
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]],
      telNbr: ['']
    },{
      validator:MustMatch("password","confirmPassword")
    })
   }
  

  ngOnInit(): void {
  }

  submitForm(formValue: any) {
    console.log("Button clicked");
    console.log("formValue", formValue);
    this.userService.addUser(formValue).subscribe(
      ()=>{
        console.log("done");
        this.router.navigate(['/admin'])
      }
    )
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  @Input() user: User;
  @Output() newUsers: EventEmitter<User[]> = new EventEmitter;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit(): void { }
  displayUser(id: number) {
    this.router.navigate([`/displayUser/${id}`]);
  }
  deleteUserById(id: number) {
    this.userService.deleteUserById(id).subscribe(
      data => {
        console.log("data deleted", data);
      }
    )
    /*this.userService.getUsers().subscribe(
      data => {
        console.log("this is my data", data);
        this.newUsers.emit(data);
      }
    )*/
  }


  editUser(id: number) {
    this.router.navigate([`/editUser/${id}`]);
  }
}

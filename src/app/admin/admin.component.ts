import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Plat } from "../models/plat";
import { PlatService } from "../services/plat.service";
import { Chef } from "../models/chef";
import { ChefService } from "../services/chef.service";
import { User } from "../models/User";
import { UserService } from "../services/user.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"],
})
export class AdminComponent implements OnInit {
  plats: Plat[];
  chefs: Chef[];
  users: User[];
  authSubs: Subscription;
  isUserAuthenticated = false;
  message: string;
  constructor(
    private router: Router,
    private platService: PlatService,
    private chefService: ChefService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.platService.getPlats().subscribe((data) => {
      console.log("Data", data);
      this.plats = data;
      this.isUserAuthenticated = this.userService.isUserAuth();
    });

    this.chefService.getChefs().subscribe((data) => {
      console.log("Data", data);
      this.chefs = data;
    });

    /*this.userService.getUsers().subscribe((data) => {
      console.log("Data", data);
      this.users = data;
    });*/
  }
  goToAddMenu() {
    this.router.navigate(["/addPlat"]);
  }

  goToAddChef() {
    this.router.navigate(["/addChef"]);
  }

  goToAddUser() {
    this.router.navigate(["/addUser"]);
  }

  updateChefs(chefs: Chef[]) {
    this.chefs = chefs;
  }

  updatePlats(plats: Plat[]) {
    this.plats = plats;
  }

  updateUsers(users: User[]) {
    this.users = users;
  }
}

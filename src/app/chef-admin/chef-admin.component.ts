import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Chef } from '../models/chef';
import { Router } from '@angular/router';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-chef-admin',
  templateUrl: './chef-admin.component.html',
  styleUrls: ['./chef-admin.component.css']
})
export class ChefAdminComponent implements OnInit {

  chefs: Chef[];
  @Input() chef: Chef;
  @Output() newChefs: EventEmitter<Chef[]> = new EventEmitter;
  constructor(private router: Router, private chefService: ChefService) { }

  ngOnInit(): void { }
  displayChef(id: string) {
    this.router.navigate([`/displayChef/${id}`]);
  }
  deleteChefById(id: string) {
    this.chefService.deleteChefById(id).subscribe(
      data => {
        console.log("data deleted", data);
      }
    )
    this.chefService.getChefs().subscribe(
      data => {
        console.log("this is my data", data);
        this.newChefs.emit(data);
      }
    )
  }


  editChef(id: string) {
    this.router.navigate([`/editChef/${id}`]);
  }
}

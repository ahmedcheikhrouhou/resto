import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Plat } from '../models/plat';
import { Router } from '@angular/router';
import { PlatService } from '../services/plat.service';


@Component({
  selector: 'app-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./plat.component.css']
})
export class PlatComponent implements OnInit {

  plats: Plat[];
  @Input() plat: Plat;
  @Output() newPlats: EventEmitter<Plat[]> = new EventEmitter;
  constructor(private router: Router, private platService: PlatService) { }

  ngOnInit(): void {
  }
  displayMenu(id: string) {
    this.router.navigate([`/displayMenu/${id}`]);
  }

  editMenu(id: string) {
    this.router.navigate([`/editPlat/${id}`]);
  }

  deletePlatById(id: string) {
    this.platService.deletePlatById(id).subscribe(
      data => {
        console.log("data deleted", data);
      }
    )
    this.platService.getPlats().subscribe(
      data => {
        console.log("this is my data", data);
        this.newPlats.emit(data);
      }
    )
  }

}

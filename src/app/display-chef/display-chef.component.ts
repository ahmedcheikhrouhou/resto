import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Chef } from '../models/chef';
import { ChefService } from '../services/chef.service';

@Component({
  selector: 'app-display-chef',
  templateUrl: './display-chef.component.html',
  styleUrls: ['./display-chef.component.css']
})
export class DisplayChefComponent implements OnInit {

  id:string;
  chef:Chef;
  constructor(private activateRoute:ActivatedRoute,private chefService:ChefService) { }

  ngOnInit(): void {
    this.id=this.activateRoute.snapshot.paramMap.get('id');
    this.chefService.getChefById(this.id).subscribe(
      data=> {
        this.chef=data[0]; 
      }
    )
  }

}

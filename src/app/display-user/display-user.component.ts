import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { UserService } from '../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-display-user',
  templateUrl: './display-user.component.html',
  styleUrls: ['./display-user.component.css']
})
export class DisplayUserComponent implements OnInit {

  id: number;
  user: User;
  constructor(private userService: UserService,
    private acttivatedRoute:ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id=+this.acttivatedRoute.snapshot.paramMap.get('id');
    this.userService.getUserById(this.id).subscribe(
      data=> {
        this.user=data; 
      }
    )
  }

}

import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm:FormGroup;

  constructor(private fb: FormBuilder) 
  {
    this.contactForm=this.fb.group({
      firstName: [''],
      lastName: [''],
      email: ['', [Validators.required, Validators.email]],
      subject:['', [Validators.required, Validators.maxLength(150), Validators.minLength(3)]],
      message:['', [Validators.required, Validators.minLength(100)]]
    })
   }

  ngOnInit(): void {
  }
  submitForm(formValue: any) {
    console.log("Button clicked");
    console.log("formValue", formValue);}
}

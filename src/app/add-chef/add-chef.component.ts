import { Component, OnInit } from '@angular/core';
import { Chef } from '../models/chef';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ChefService } from '../services/chef.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-chef',
  templateUrl: './add-chef.component.html',
  styleUrls: ['./add-chef.component.css']
})
export class AddChefComponent implements OnInit {

  chef:Chef;
  chefForm:FormGroup;
  constructor(private fb:FormBuilder,private chefService:ChefService,private router:Router) { }

  ngOnInit(): void {
    this.chef=new Chef('','','','');
    this.chefForm=this.fb.group({
      name:[''],
      price:[''],
      description:[''],
      img: ['']
    })
  }

  addChef(){
    console.log('this is my form',this.chef);
    this.chefService.addChef(this.chef).subscribe(
      data=>{
        console.log("chef added succesfully");
        this.router.navigate(['/admin']);
      }
    )
  }

}

import { Component, OnInit } from "@angular/core";
import { User1 } from "../models/user1";

@Component({
  selector: "app-data",
  templateUrl: "./data.component.html",
  styleUrls: ["./data.component.css"],
})
export class DataComponent implements OnInit {
  users: User1[];
  constructor() {}

  ngOnInit(): void {
    this.users = [
      {
        nom: "AHMED",
        prenom: "cheikhrouhou",
        age: 26,
        email: "ahmed.cheikhrouhou@gmail.com",
        dateDeNaissance: new Date(),
        password: "12345678",
      },
      {
        nom: "khaled",
        prenom: "ben hamida",
        age: 30,
        email: "khaled.benhamida@gmail.com",
        dateDeNaissance: new Date(),
        password: "12345678",
      },
      {
        nom: "abdelrahmen",
        prenom: "masmoudi",
        age: 26,
        email: "abdelrahmen.masmoudi@gmail.com",
        dateDeNaissance: new Date(),
        password: "12345678",
      },
      {
        nom: "lilia",
        prenom: "ben ameur",
        age: 26,
        email: "lilia.benameur@gmail.com",
        dateDeNaissance: new Date(),
        password: "12345678",
      },
      {
        nom: "aref",
        prenom: "cheikhrouhou",
        age: 26,
        email: "aref.cheikhrouhou@gmail.com",
        dateDeNaissance: new Date(),
        password: "12345678",
      },
    ];
  }
}

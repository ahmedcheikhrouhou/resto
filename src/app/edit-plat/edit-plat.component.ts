import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { Router, ActivatedRoute } from '@angular/router';
import { PlatService } from '../services/plat.service';

@Component({
  selector: 'app-edit-plat',
  templateUrl: './edit-plat.component.html',
  styleUrls: ['./edit-plat.component.css']
})
export class EditPlatComponent implements OnInit {

  id:string;
  plat:Plat;
  name:any;
  description:any;
  price:any;
  img:any;
  constructor(private activatedRoute:ActivatedRoute,private platService:PlatService,private router:Router) { 
   
  }


  ngOnInit(): void {
    this.id=this.activatedRoute.snapshot.paramMap.get('id');
    this.platService.getPlatById(this.id).subscribe(
      data=> {
        console.log("this is my data",data);

        this.plat = data[0]; 
       
        
      }
    )
    
  }

  editMenu(p:any){
    this.platService.editPlat(this.plat).subscribe(
      ()=>
      this.router.navigate(['/admin'])
    )
  }

}

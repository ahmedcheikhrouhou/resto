import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PlatService } from '../services/plat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-plat',
  templateUrl: './add-plat.component.html',
  styleUrls: ['./add-plat.component.css']
})
export class AddPlatComponent implements OnInit {

  menu:Plat;
  menuForm:FormGroup;
  imagePreview : string;
  constructor(private fb:FormBuilder, private platService:PlatService,private router:Router) { }

  ngOnInit() 
  {
    this.menu=new Plat('','',0,'','');
    this.menuForm=this.fb.group({
      name:[''],
      price:[''],
      description:[''],
      img: ['']
    })
  }
  addMenu(){
    console.log('this is my form',this.menu);
    this.platService.addPlat(this.menu,this.menuForm.value.img).subscribe(
      data=>{
        console.log("plat added succesfully");
        this.router.navigate(['/admin']);
      }
    )
  }
  onImageSelected(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.menuForm.patchValue({img: file});
    this.menuForm.updateValueAndValidity();
    console.log("This is my file", file);
    console.log("This is my form", this.menuForm);
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview =  reader.result as string
    };
    reader.readAsDataURL(file);
    
  }

}

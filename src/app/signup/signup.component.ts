import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MustMatch } from '../validators/mustmatchvalidator';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  userForm: FormGroup;
  constructor( 
    private fb: FormBuilder,
    private userService:UserService,
    private router:Router
   ) { 
    this.userForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]],
      telNbr: ['']
    },{
      validator:MustMatch("password","confirmPassword")
    })
   }
  ngOnInit() {
  }
  submitForm(formValue: any) {
    console.log("Button clicked");
    console.log("formValue", formValue);
    this.userService.addUser(formValue).subscribe(
      ()=>{
        console.log("done");
        this.router.navigate(['/admin'])
      }
    )
  }
}

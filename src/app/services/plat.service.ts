import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plat } from '../models/plat';

@Injectable({
  providedIn: 'root'
})
export class PlatService {

  private platUrl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }

  getPlats(): Observable<Plat[]> {
    const url=`${this.platUrl}api/plats`;
    console.log("url of get plats",url);
    return this.http.get<Plat[]>(url);
  }

  addPlat(plat: Plat , image:File): Observable<Plat> {
    const url = `${this.platUrl}api/plats`;
    let formData = new FormData();
    formData.append('name' , plat.name);
    formData.append('description' , plat.description);
    formData.append('price' , String(plat.price));
    formData.append('img' ,image , plat.name);
    console.log("url of add plat", url);

    return this.http.post<Plat>(url, formData)
  }
  getPlatById(id: string): Observable<Plat> {
    const url = `${this.platUrl}api/plats/${id}`;
    return this.http.get<Plat>(url);
  }

  deletePlatById(id: string): Observable<Plat> {
    const url = `${this.platUrl}api/plats/${id}`;
    return this.http.delete<Plat>(url);
  }



  editPlat(plat: Plat): Observable<Plat> {
    const url =`${this.platUrl}api/plats/${plat._id}`
    return this.http.put<Plat>(url, plat)
  }
}

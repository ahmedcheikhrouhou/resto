import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chef } from '../models/chef';

@Injectable({
  providedIn: 'root'
})
export class ChefService {

  private chefUrl="http://localhost:3000/";
  constructor( private http:HttpClient) { }

  getChefs(): Observable<Chef[]>{
    const url=`${this.chefUrl}api/chefs`;
    console.log("url of get chefs",url);
    return this.http.get<Chef[]>(url)
  }
  getChefById(id:string):Observable<Chef>{
    const url= `${this.chefUrl}api/chefs/${id}`;
    return this.http.get<Chef>(url);
  }

  deleteChefById(id:string):Observable<Chef>{
    const url=`${this.chefUrl}api/chefs/${id}`;
    return this.http.delete<Chef>(url);
  }

  addChef(chef:Chef):Observable<Chef>{
    const url = `${this.chefUrl}api/chefs`;
    console.log("url of add chef", url);
    return this.http.post<Chef>(url,chef)
  }

  editChef(chef:Chef):Observable<Chef>{
    const url =`${this.chefUrl}api/chefs/${chef._id}`
    return this.http.put<Chef>(url,chef)
  }
}

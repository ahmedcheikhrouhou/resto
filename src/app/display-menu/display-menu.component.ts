import { Component, OnInit } from '@angular/core';
import { Plat } from '../models/plat';
import { ActivatedRoute } from '@angular/router';
import { PlatService } from '../services/plat.service';

@Component({
  selector: 'app-display-menu',
  templateUrl: './display-menu.component.html',
  styleUrls: ['./display-menu.component.css']
})
export class DisplayMenuComponent implements OnInit {

  plat:Plat;
  id:string;
  constructor(private activateRoute:ActivatedRoute,
    private PlatService:PlatService) { }

  ngOnInit(): void {
    this.id=this.activateRoute.snapshot.paramMap.get('id');
    this.PlatService.getPlatById(this.id).subscribe(
      data=> {
        console.log(data);
        
        this.plat=data[0]; 
      }
    )
    }
}

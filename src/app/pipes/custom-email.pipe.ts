import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customEmail'
})
export class CustomEmailPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    let result='';
    result = value.replace(/a|i|e/gi,'*');
    return result;
  }

}

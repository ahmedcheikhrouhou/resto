import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { ExclusiveItemsComponent } from './exclusive-items/exclusive-items.component';
import { HistoryComponent } from './history/history.component';
import { DemoComponent } from './demo/demo.component';
import { FoodMenuComponent } from './food-menu/food-menu.component';
import { ChefsComponent } from './chefs/chefs.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ExclusiveItemComponent } from './exclusive-item.component/exclusive-item.component';
import { FooterComponent } from './footer/footer.component';
import { PlatComponent } from './plat/plat.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import{ReactiveFormsModule,FormsModule} from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { AddPlatComponent } from './add-plat/add-plat.component';
import { HomeMenuComponent } from './home-menu/home-menu.component';
import { DisplayMenuComponent } from './display-menu/display-menu.component';
import { ChefComponent } from './chef/chef.component'
import { PlatService } from './services/plat.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UsersComponent } from './users/users.component';
import { UserService } from './services/user.service';
import { ChefService } from './services/chef.service';
import { ChefAdminComponent } from './chef-admin/chef-admin.component';
import { DisplayChefComponent } from './display-chef/display-chef.component';
import { AddChefComponent } from './add-chef/add-chef.component';
import { EditPlatComponent } from './edit-plat/edit-plat.component';
import { AddUserComponent } from './add-user/add-user.component';
import { DisplayUserComponent } from './display-user/display-user.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AuthInterceptor } from './services/auth-interceptor-service.service';
import { AuthGuardService } from './services/auth-guard.service';
import { DataComponent } from './data/data.component';
import { CustomEmailPipe } from './pipes/custom-email.pipe';
import { BackgroundDirective } from '../directives/background.directive';
import { ReversePipe } from './pipes/reverse.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    ExclusiveItemsComponent,
    HistoryComponent,
    DemoComponent,
    FoodMenuComponent,
    ChefsComponent,
    ReservationComponent,
    ExclusiveItemComponent,
    FooterComponent,
    PlatComponent,
    SigninComponent,
    SignupComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    AdminComponent,
    AddPlatComponent,
    HomeMenuComponent,
    DisplayMenuComponent,
    ChefComponent,
    UsersComponent,
    ChefAdminComponent,
    DisplayChefComponent,
    AddChefComponent,
    EditPlatComponent,
    AddUserComponent,
    DisplayUserComponent,
    EditChefComponent,
    EditUserComponent,
    DataComponent,
    CustomEmailPipe,
    BackgroundDirective,
    ReversePipe,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,

  ],
  providers: [
    PlatService,
    UserService,
    ChefService,
    AuthGuardService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class User{
    constructor(
        public id : number,
        public firstName : string,
        public lastName : string,
        public email: string,
        public telNbr: number,
        public password: string,
        public confirmPassword: string
    ){}
}
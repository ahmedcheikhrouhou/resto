export class Chef{
    constructor(
        public _id : string,
        public name : string,
        public status: string,
        public img: string
        ){}
}
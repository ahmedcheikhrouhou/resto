export class Plat{
    constructor(
        public _id : string,
        public name : string,
        public price : number,
        public description: string,
        public img: string
        ){}
}
const mongoose = require('mongoose');

const chefSchema = mongoose.Schema({
    name: String,
    status: String,
    img: String
});

const chef = mongoose.model('Chef', chefSchema);

module.exports = chef;
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')
const userSchema = mongoose.Schema({
    email: { type: String, required: true, unique: true },
    lastName: String,
    firstName: String,
    password: String
});

userSchema.plugin(uniqueValidator);
const user = mongoose.model('User', userSchema);

module.exports = user;
const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, "secret_key");
        console.log("token in auth.js", token);
        next();

    } catch {
        res.status(401).json({
            message: 'auth failed'
        })
    }
}

// const jwt = require('jsonwebtoken');
// module.exports = (req, res, next) => {

//     try {
//         // req {
//         //     // headers: {
//         //     //     authorization: 'Bearer bjclbvjerjvjevlkefvjeflkvbjfvlbfevjkfdlvbjdfvkdbjvljb'
//         //     // }
//         // body{
//         // 
//         // }
//         // }
//         const token = req.headers.authorization.split(" ")[1];
//         jwt.verify(token, 'secret_key');
//         console.log("Token in auth.js", token);
//         next();
//     } catch {
//         res.status(401).json({
//             message: 'Auth failed'
//         })
//     }
// }
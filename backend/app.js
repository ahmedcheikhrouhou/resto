const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Plat = require("./models/plat");
const Chef = require("./models/chef");
const userRoutes = require("./routes/user");
const auth = require("./auth/auth");
const path = require("path");
const multer = require("multer");
mongoose.connect("mongodb://localhost:27017/projectRestoDB", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/images', express.static(path.join('backend/images')));

const MIME_TYPE = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'

}
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MIME_TYPE[file.mimetype];
        let error = new Error("mime type is invalid");
        if (isValid) {
            error = null;
        }
        cb(null, 'backend/images')
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(' ').join('-');
        const extension = MIME_TYPE[file.mimetype];
        const imgName = name + '-' + Date.now() + '.' + extension;
        cb(null, imgName)
    }
})

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, Accept, Content-Type, X-Requested-with, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, DELETE, OPTIONS, PATCH, PUT"
    );
    next();
});

app.post("/api/plats", multer({ storage: storage }).single('img'), (req, res) => {
    //adding plat to data base
    console.log();

    url = req.protocol + '://' + req.get("host");
    const plat = new Plat({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        img: url + '/images/' + req.file.filename
    });
    console.log("here is my plat", plat);

    plat.save();

    res.status(201).json({
        message: "done, plat added succefuly",
    });
});

app.get("/api/plats", auth, (req, res) => {
    Plat.find((err, documents) => {
        if (err) {
            console.log("error", err);
        } else {
            console.log(documents);

            res.status(200).json(documents);
        }
    });
});

//get plat by id
app.get("/api/plats/:id", (req, res) => {
    Plat.find({ _id: req.params.id }).then((result) => {
        console.log("fetshed plat from db", result);
        res.status(200).json(result);
    });
});

//edit plat (update plat)
app.put("/api/plats/:id", (req, res) => {
    const plat = new Plat({
        _id: req.body._id,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        img: req.body.img,
    });
    Plat.updateOne({ _id: req.body._id }, plat).then((result) => {
        if (result) {
            res.status(200).json({
                message: "updated succefully",
            });
        } else {
            console.log("error in update");
        }
    });
});

//delete plat by id
app.delete("/api/plats/:id", (req, res) => {
    Plat.deleteOne({ _id: req.params.id }).then((plat) => {
        console.log("deleted succefuly", result);
    });

    res.status(200).json(plat);
});

//add new chef
app.post("/api/chefs", (req, res) => {
    //adding chef to data base
    const chef = new Chef({
        name: req.body.name,
        status: req.body.status,
        img: req.body.img,
    });
    console.log("here is my chef", chef);

    chef.save();

    res.status(201).json({
        message: "done, chef added succefuly",
    });
});
//get all chefs
app.get("/api/chefs", auth, (req, res) => {
    Chef.find((err, documents) => {
        if (err) {
            console.log("error", err);
        } else {
            console.log(documents);

            res.status(200).json(documents);
        }
    });
});

//get chef by id  
app.get("/api/chefs/:id", (req, res) => {
    Chef.find({ _id: req.params.id }).then((result) => {
        console.log("fetshed chef from db", result);
        res.status(200).json(result);
    });
});

//delete chef by id
app.delete("/api/chefs/:id", (req, res) => {
    Chef.deleteOne({ _id: req.params.id }).then((chef) => {
        console.log("deleted succefuly", result);
    });

    res.status(200).json(chef);
});

//edit chef 
app.put("/api/chefs/:id", (req, res) => {
    const chef = new Chef({
        _id: req.body._id,
        name: req.body.name,
        status: req.body.status,
        img: req.body.img
    });
    Chef.updateOne({ _id: req.body._id }, chef).then((result) => {
        if (result) {
            res.status(200).json({
                message: "updated succefully",
            });
        } else {
            console.log("error in update");
        }
    });
});

app.use('/api/users', userRoutes);
module.exports = app;
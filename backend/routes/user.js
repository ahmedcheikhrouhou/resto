const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const User = require("../models/user");
const jwt = require("jsonwebtoken");
router.post("/signup", (req, res) => {
    bcrypt.hash(req.body.password, 10).then((hash) => {
        console.log("hash", hash);
        console.log("req from signup", req.body.password);
        const user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hash,
        });

        user
            .save()
            .then((result) =>
                res.status(201).json({
                    message: "user added succefuly",
                    result: result,
                })
            )
            .catch((err) => {
                console.log(err);

                res.status(500).json({
                    error: err,
                });
            });
    });
});

router.post("/login", (req, res) => {
    // Traitement Login
    console.log("Req body", req.body);
    let gettedUser;
    User.findOne({ email: req.body.email })
        .then((user) => {
            if (!user) {
                // 401 : authentification failed
                // 404 : not found
                res.status(401).json({
                    message: "Auth failed",
                });
            }
            gettedUser = user;
            return bcrypt.compare(req.body.password, gettedUser.password);
        })
        .then((result) => {
            if (!result) {
                return res.status(401).json({
                    message: "Auth failed",
                });
            }
            //generate token
            const token = jwt.sign({ email: gettedUser.email, userId: gettedUser._id },
                "secret_key", { expiresIn: "1h" }
            );
            res.status(200).json({
                message: "User finded",
                token: token,
                expiresIn: 3600
            });
        })
        .catch((err) => {
            console.log("err", err);
            res.status(401).json({
                message: "Auth failed",
            });
        });
});

module.exports = router;